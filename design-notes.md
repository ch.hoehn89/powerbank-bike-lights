---
title: Design Notes
...

# Notes

## Mounts

- not meant for plug'n'play
	- zip ties for easy installation
- cable holes should ensure ventilation but prevent mud
- perspex windows can be glued
	+ ø = 30mm is hard to get; however, custom cuts are quite effortable (e.g. <http://www.acrylformen.de/shop/de/Plexi-/-Acrylglaszuschnitt/Rund-/-Kreis/Plexiglas-GS-Satiniert-Farbig/Plexiglas-Rund-SATINICE-Weiss-snow-3-mm-Stark->)

## LEDs

- 3W high power LED white, star model
	+ https://www.ebay.de/itm/223012126314
	* 10000k - 20000k white
	* 750mA
	* 3,5 V bis 4,5 V
	* 240lm -260lm (120°)
	* at a 5V supply: $ R_{white} = \frac{(5-3.5…4.5)V}{0.75A}= 0.67…2 \Omega $
		- I used 1R || 2R2
- 3W high power LED red, star model
+ https://www.ebay.de/itm/323767523967
	* red, 610nm - 630nm
	* 700mA
	* 2,2 V bis 2,8 V
	* 60lm - 70lm (120°)
	* at a 5V supply: $ R_{red} = \frac{(5-2.2…2.8)V}{0.7A}= 3.14…4 \Omega $
		- I used 1R7 || 10R

## Cables

I put both cables in one shift cable housing, however the only cables I had available that would still fit both inside that housing had a copper diameter of 0.15 mm.

At ~0.5 m cable length this results into a voltage drop of ~3.5 %.

# Inspirations

- https://www.thingiverse.com/thing:195179
- https://www.thingiverse.com/thing:3091337
- https://www.thingiverse.com/thing:2965812
- https://www.thingiverse.com/thing:405919
- https://www.thingiverse.com/thing:3651
