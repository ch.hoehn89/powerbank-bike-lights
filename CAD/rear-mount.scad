glassDiameter = 30;
t_glass = 3; //thickness of front glass
t_glue = 1.5; //gluing width

D_led = 21;
h_led = 2.1;
tol_led = 1;
scatterDistance = 10; //optical distance to get scattered light

D_rod = 27 + 1; //diameter of the saddle rod + rubber insert thickness
angle_rod = 18; //degrees of rod angle normal to ground

t_mount = 2 + t_glue; //wall thickness

t_cable = 1.1; //outer diameter of cables
D_shift = 5; //outer diameter of shift cable mounting

t_zip = 0.5; //zip tie thickness
w_zip = 8; //zip tie width

render = 200;
$fn=render;

union(){
    difference()
    {
        difference()
            {
            cylinder(
                h = h_led + tol_led + scatterDistance + t_mount + (D_led/2 + tol_led + t_mount)*abs(sin(angle_rod)) + t_mount + D_rod/2,
                r1 = D_led/2 + tol_led + t_mount*2,
                r2 = glassDiameter/2 + t_mount - t_glue); //LED casing
            translate([0,0,h_led + tol_led + scatterDistance + (D_led/2 + tol_led + t_mount)*abs(sin(angle_rod)) + t_mount + D_rod/2])
                cylinder(
                    h = t_glass+t_mount,
                    d = glassDiameter + 0.2);  //added tolerance for easier assembly, cut glass mounting
            translate([0,0,t_mount + (D_led/2 + tol_led + t_mount)*abs(sin(angle_rod)) + t_mount + D_rod/2])
                cylinder(
                    h = h_led + tol_led + scatterDistance,
                    r = D_led/2 + tol_led); //cut optical space for LED
            };
         rotate([0,90+angle_rod,0])
            cylinder(
                h = D_rod*2,
                d = D_rod,
                center = true); //cut rod

        rotate([90,0,90])
            rotate_extrude(angle = 180)
                translate([D_rod/2 + t_mount*2,0,0])
                union(){
                    translate([0.5,0,0]) scale([0.2,1])
                    circle(r=w_zip/2); //cut zip tie hole
                    square([t_zip*2,w_zip],true);}

        translate([0,D_led/2 + tol_led + 1,t_mount + (D_led/2 + tol_led + t_mount)*abs(sin(angle_rod)) + t_mount + D_rod/2-1]) rotate([180+50,0,0])
            cylinder(
            h = t_mount*10,
            d = D_shift + 0.1); //hole for shift cable housing
        translate([0,D_led/2 + tol_led -1,t_mount + (D_led/2 + tol_led + t_mount)*abs(sin(angle_rod)) + t_mount + D_rod/2 +1]) rotate([180+50,0,0])
            cylinder(
            h = t_mount*10,
            d = t_cable*2.1); //hole for cables
    }
    translate([14,0,25]) rotate([90,0,90])
        linear_extrude(5)
            text("moe's",
            size = 5,
            font = "CMUserif:style=Bold",
            halign = "center",
            valign = "center");
}
translate([0,0,50])
    %cylinder(h=t_glass, d=glassDiameter); //front glass ghost
rotate([0,90+angle_rod,0])
            %cylinder(
                h = D_rod*2,
                d = D_rod,
                center = true); //rod ghost